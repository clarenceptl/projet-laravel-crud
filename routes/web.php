<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\WallController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/films',[FilmController::class, 'index'])
   ->middleware(['auth'])->name('films');

Route::get('/film/{id_film}',[FilmController::class, 'show'])
   ->middleware(['auth'])->name('film');

Route::get('/wall',[WallController::class, 'index'])
   ->middleware(['auth'])->name('wall');

Route::post('/wall',[WallController::class, 'create'])
->middleware(['auth'])->name('post_wall');

// {param?} ?-> rend le param optionnel
Route::get('/plop/{param?}', function ($param = null) {
    echo('plop : '.$param);
})->middleware(['auth'])->name('plop');

Route::get('/updatePost/{id_post}',[WallController::class,'update'])->middleware(['auth'])->name('post_update');

Route::post('/savePost',[WallController::class,'save'])->middleware(['auth'])->name('post_save');

Route::get('/deletePost/{id_post}',[WallController::class,'delete'])->middleware(['auth'])->name('post_delete');

Route::get('/plop/{param?}', function ($param = null) {
    echo('plop : '.$param);
})->middleware(['auth'])->name('plop');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
// middleware check si on est bien connecté

require __DIR__.'/auth.php';
