<?php

namespace App\Http\Controllers;

use App\Models\Film;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    function index()
    {
        //affiche la liste des films
        $films = Film::paginate(50);

        return view('films',['films'=>$films]);
    }

    function show(Request $request)
    {
        //affiche un film spécifique
        $film = Film::find($request->id_film);
        return view('film',['film'=>$film]);
    }
}
