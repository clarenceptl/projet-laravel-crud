<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WallController extends Controller
{
    // affiche le form et les post du wall
    function index()
    {
        //recupère les post (5 max)
        $posts=Post::paginate(5);
        //recupère tous les post
        // $posts=Post::all();

        return view('wall',['posts'=>$posts]);
    }
    // recevoir le form et save un post en bdd
    function create(Request $request)
    {
        $post=new Post;
        $post->content = $request->content;
        $post->user_id = Auth::id();
        $post->save();
        return redirect('wall')->with('status','Post created');;
    }
    //update post
    function update(Request $request)
    {
        $post=Post::find($request->id_post);

        return view('updatePost',['post'=>$post]);
    }

     // met a jour le post
     function save(Request $request)
     {
        $post=Post::find($request->id);
        $post->content=$request->content;
        $post->save();
        return redirect('wall')->with('status','Post updated');
     }


    // delete post
    function delete(Request $request)
    {
        $post=Post::find($request->id_post);
        $post->delete();

        return redirect('wall')->with('status','Post deleted');
    }
}
